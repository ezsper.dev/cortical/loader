import * as FacebookDataLoader from 'dataloader';
import { BaseContext } from '@cortexql/core';

export abstract class Loader<K, V> implements FacebookDataLoader<K, V> {

  options: FacebookDataLoader.Options<K, V>;

  constructor(public context: BaseContext) {
    this.load = this.load.bind(this.loader);
    this.loadMany = this.loadMany.bind(this.loader);
    this.clear = this.clear.bind(this.loader);
    this.clearAll = this.clearAll.bind(this.loader);
    this.prime = this.prime.bind(this.loader);
  }

  protected abstract batchLoad(keys: K[]): Promise<(V | Error)[]>;

  loader = new FacebookDataLoader(this.batchLoad.bind(this), this.options);
  load = this.loader.load;
  loadMany = this.loader.loadMany;
  clear = this.loader.clear;
  clearAll = this.loader.clearAll;
  prime = this.loader.prime;

}
