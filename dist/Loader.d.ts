import * as FacebookDataLoader from 'dataloader';
import { BaseContext } from '@cortexql/core';
export declare abstract class Loader<K, V> implements FacebookDataLoader<K, V> {
    context: BaseContext;
    options: FacebookDataLoader.Options<K, V>;
    constructor(context: BaseContext);
    protected abstract batchLoad(keys: K[]): Promise<(V | Error)[]>;
    loader: FacebookDataLoader<K, V>;
    load: (key: K) => Promise<V>;
    loadMany: (keys: K[]) => Promise<V[]>;
    clear: (key: K) => FacebookDataLoader<K, V>;
    clearAll: () => FacebookDataLoader<K, V>;
    prime: (key: K, value: V) => FacebookDataLoader<K, V>;
}
