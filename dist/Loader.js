"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FacebookDataLoader = require("dataloader");
class Loader {
    constructor(context) {
        this.context = context;
        this.loader = new FacebookDataLoader(this.batchLoad.bind(this), this.options);
        this.load = this.loader.load;
        this.loadMany = this.loader.loadMany;
        this.clear = this.loader.clear;
        this.clearAll = this.loader.clearAll;
        this.prime = this.loader.prime;
        this.load = this.load.bind(this.loader);
        this.loadMany = this.loadMany.bind(this.loader);
        this.clear = this.clear.bind(this.loader);
        this.clearAll = this.clearAll.bind(this.loader);
        this.prime = this.prime.bind(this.loader);
    }
}
exports.Loader = Loader;
//# sourceMappingURL=Loader.js.map